from .redis.simple_exists_stored_key import exists_stored_key, get_keys_list
from .redis.simple_test import is_redis_available_for_data
from .redis.handle_list_of_values import get_all_values_from_set,\
    append_many_values_right, append_many_values_left
from .redis.simple_flush_this import delete_all_keys_current
from .redis.simple_set_key_value import set_single_key_value, get_single_key_value

from .constants import KeyNames


def check_datastore_availability(_logger, host, port, datab, db_type="REDIS"):
    #global logger
    logger = _logger
    logger.debug("Ready to check .... Datastore connection")

    if db_type.upper() == "REDIS":
        #global client
        logger.debug("Saving data to REDIS store ... Is supported !")

        try:
            client = is_redis_available_for_data(logger, host, port, datab)
        except Exception as conn_e:
            # Failure
            logger.error("??? Connection with Redis Server Failed ???\n"+
                         "Reason is ... %s", str(conn_e))
            
            return None 

        return client
    else:
        raise NotImplementedError("CheckDataStoreAvailability only for Redis !")

def check_key_existence(key, client, db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client
        response = exists_stored_key(client=client, key=key)

        return response
    else:
        raise NotImplementedError("CheckKeyExistence only for Redis !")

def get_all_values(key, client, db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client

        response = get_all_values_from_set(client, key)

        return response
    else:
        raise NotImplementedError("GetAllValuesFromSet only for Redis !")

def append_end(key, client, values, db_type):
    if db_type.upper() == "REDIS":
        #global client

        #logger.debug("1 --> %s\n%s", key, values)
        append_many_values_right(client, key, values)
    else:
        raise NotImplementedError("AppendEnd only for Redis !")


def append_start(key, values, client, db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client

        #logger.debug("Ready  --> %s\n%s", key, values)
        append_many_values_left(client, key, values)
    else:
        raise NotImplementedError("AppendStart only for Redis !")


def delete_data_from_db(client, db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client

        #logger.debug("Ready to Delete ALL Keys from Current DB !")
        response = delete_all_keys_current(client)

        return response
    else:
        raise NotImplementedError("DeleteDataFromDB only for Redis !")

def save_open_ports_for_server(logger, server_id, ports_list, client, db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client

        # Check Parent Key Exists
        parent_key = KeyNames().openports
        server_key = parent_key + ":" + server_id

        logger.debug(" --> %s .... %s", server_key, ports_list)

        # Save (key)Server-ID : (value)Open-Ports
        response = set_single_key_value(client,
                                        key=server_key,
                                        value=ports_list)

        if not response:
            # Failure .. save open-ports to Datastore
            logger.error("Failed to store open-ports for server ... %s",
                         server_id)
    else:
        raise NotImplementedError("SaveOpenPorts4Server only for Redis !")


def get_open_ports_for_server(logger, server_id, client, db_type="REDIS"):
    '''
    What ports on a Linux server are open ... get a list of
    '''
    if db_type.upper() == "REDIS":
        #global client

        logger.debug("Ready to get a list of open ports on linux server ... %s", server_id)

        key = KeyNames().openports + ":" + server_id

        if not check_key_existence(key, client):
            #raise Exception( "Not Found Key ... {}".format( key ) )
            logger.debug(
                "Not Found Key for Open-Ports List in Datastore ... First Time Run ?"
            )
            return None

        # Key exists ... find all elements
        #return get_all_members_of_set( client, key )

        response = get_single_key_value(client, key)
        if not response:
            # Failure retrieving values from datastore
            logger.debug(
                "Querying Datastore for Open-Ports of Server %s .... Empty ResultSet",
                key)

            return None

        #logger.debug("Get VALUES response ... %s", response )
        return response
    else:
        raise NotImplementedError("GetOpenPorts4Server only for Redis !")

def my_keys_list(client, pattern="*", db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client

        return get_keys_list(client, pattern)
    else:
        raise NotImplementedError("MyKeysList only for Redis !")

def get_stored_hosts_up(client, db_type="REDIS"):
    if db_type.upper() == "REDIS":
        #global client

        hosts_up = KeyNames().hostsup

        return get_single_key_value(client, key=hosts_up)
    else:
        raise NotImplementedError("GetStoredHostsUp only for Redis !")
