import redis



def establish_single_connection(logger, host, port, db_number):
    logger.debug("Trying to establish a network connection with Redis")
    try:
        client = redis.StrictRedis(host=host, port=port, db=db_number)
    except redis.ConnectionError as conn_err:
        logger.error("Failed Redis Connection ... %s", str(conn_err))
        return None

    logger.debug("Successful Connection & Client is %s", client)

    # The test
    ping = client.ping()

    logger.debug("Successful Test & Ping is %s", ping)
    logger.debug("Redis Server Info is ['Version']=%s",
                 client.info().get("redis_version"))

    return client

def establish_pool_connection():
    pass
